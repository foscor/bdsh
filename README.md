# README #

Epitech's project in shell.

### BDSH ###

DataBase in shell.

### Synopsys ###

bdsh.sh [-k] [-f <db_file>] (put (<clef> | $<clef>) (<valeur> | $<clef>) |                                                         
                                del (<clef> | $<clef>) [<valeur> | $<clef>] |                                                               
                                select [<expr> | $<clef>] |                                                                                 
                                flush)
