#!/bin/bash

#       BDSH 
# EPITECH'S PROJECT
#
#
# By Foscor 'William Tahar'
#
# January 2015


arg_k=0				# Etat de l'argument -k
name_file="sh.db"		# Nom du fichier par defaut
sep='#tatatitadikecmal#'	# Séparateur

# Supprime le contenu du fichier en param
my_flush() {
    if [ -f $name_file ]; then
	echo -e "" > $name_file
    fi
    exit
}

# Message d'erreur --> nom de clé invalide
my_nokey() {
    echo "No such key : $key"
    exit
}

# Fonction select prend en param : $key $name_file
my_select() {
    if [ ${key:0:1} == '$' ]; then           # MODE KEY                                                                                                                                                                                      
        echo "Mode key"
    else                                        #MODE SIMPLE                                                                                                                                                                                 
	mgrep="$key $sep"
        if [ -e $name_file ]; then
            result_grep=$(grep "$mgrep" $name_file)
        else
            result_grep=""
        fi
	
        if (( ${#result_grep} >= 1 )); then
	    new_value=$(echo -e $result_grep | sed "s/^$key $sep //")
	    echo "$new_value"
	else
	    my_nokey $key
	fi
    fi
}

# Fonction my_del prend en param : $key $name_file $valeur
my_del() {
    if [ ${key:0:1} == '$' ]; then           # MODE KEY                                                                                                                                                                                   
        echo "Mode key"
    else					#MODE SIMPLE
	mgrep="$key $sep"
        if [ -e $name_file ]; then
            result_grep=$(grep "$mgrep" $name_file)
        else
            result_grep=""
        fi
	
	if (( ${#result_grep} >= 1 )); then

	    size=$(( ${#key} + ${#sep} + 2 ))
	    str=${result_grep:0:$size}

	    if [ "$result_grep" == "$key $sep $valeur" ] && (( ${#valeur} >= 0 )); then
		stock_file=$(sed "/^$result_grep/d" $name_file)
		echo "$stock_file" > $name_file
	    else
		stock_file=$(sed "/^$result_grep/d" $name_file)
		echo "$stock_file" > $name_file
		size=$(( ${#key} + ${#sep} + 2 ))
		str=${result_grep:0:$size}
		echo "str : $size"
		echo "$str" >> $name_file
	    fi
	else
	    my_nokey $key
	fi
    fi
}

# Fonction my_put prend en param : $key $name_file $valeur
my_put() {	

    if [ ${key:0:1} == '$' ]; then		# MODE KEY

	key=${key:1}
	mgrep="$key $sep"
	
	if [ -e $name_file ]; then
            result_grep=$(grep "$mgrep" $name_file)
	else
            result_grep=""
	fi
	
	if (( ${#result_grep} >= 1 )); then

	    new_value=$(echo -e $result_grep | sed "s/^$key $sep //")

	    result_grep2=$(grep "$new_value $sep" $name_file)

	    if (( ${#result_grep} >= 1 )); then
		stock_file=$(sed "/^$new_value $sep/d" $name_file)
		echo "$stock_file" > $name_file
		echo "$new_value $sep $valeur" >> $name_file
	    else
		echo "$new_value $sep $valeur" >> $name_file
	    fi
	else
	    my_nokey $key
	fi
    else					# MODE SIMPLE
	mgrep="$key $sep"
	
	if [ -f $name_file ]; then
            result_grep=$(grep "$mgrep" $name_file)
	else
            result_grep=""
	fi
	
	if (( ${#result_grep} >= 1 )); then
	    
	    stock_file=$(sed "/^$key $sep/d" $name_file)
	    echo "$stock_file" > $name_file
	    echo "$key $sep $valeur" >> $name_file
	else
            echo "$key $sep $valeur" >> $name_file
	fi
    fi
}

# Check si il y a des arguments
if (( $# == 0 )); then
echo -e "bdsh.sh [-k] [-f <db_file>] (put (<clef> | $<clef>) (<valeur> | $<clef>) |
                                del (<clef> | $<clef>) [<valeur> | $<clef>] |
                                select [<expr> | $<clef>] |
				flush)"
exit
fi

# Parcours les arguments
while [ "$1" != "" ]
do
    if [ $1 == "-k" ]; then
        arg_k=1
    elif [ $1 == "-f" ]; then
	shift
	name_file=$1
    elif [ $1 == "put" ]; then
	shift
	key=$1
        shift
        valeur=$1	
	my_put $name_file $key $valeur
    elif [ $1 == "del" ]; then
	shift
	key=$1
	shift
	valeur=$1
	my_del $key $valeur $name_file
    elif [ $1 == "select" ]; then
	shift
	key=$1
	my_select $key $name_file
    elif [ $1 == "flush" ]; then
	my_flush $name_file
    else
	echo "Syntax error : Usage $1"
	exit
    fi
    shift
done

# Check param -k
if (( arg_k == 1 )); then
    echo "$key=$valeur"
fi
